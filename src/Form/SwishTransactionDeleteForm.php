<?php

namespace Drupal\swish_payment\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Swish transaction entities.
 *
 * @ingroup swish_payment
 */
class SwishTransactionDeleteForm extends ContentEntityDeleteForm {


}
